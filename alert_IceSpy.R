
rm(list = ls())

# 1. Enter path data, and muslim days ----------------------------------------

path_csv <- "data/Alarms monthly 2024-07.csv"


## muslim days
Ramadan <- "2024-04-10"
Tabasky <- "2024-06-16"

# 2. Library -------------------------------------------------------------------
pacman::p_load(tidyverse, lubridate, kableExtra, timeDate, readxl)

# 3. Importing database --------------------------------------------------------

# database <- read_excel(path_csv, skip = 5)
database <- read.csv2(path_csv, skipNul = TRUE, skip = 5)

year <- as.numeric(str_extract(path_csv, "20[0-9]{2}"))
yearmonth <- str_extract(path_csv, "20[0-9]{2}-[0-9]{2}")
month <- as.numeric(str_extract(yearmonth, "[0-9]{2}$"))

names(database) <- c("alarm_dtte_x", "alarm_type", "sonde", "nrserie", "zone", "site",
                     "channel_type", "canal", "nr_channel", "value", "alarm_limit", "acquit_by",
                     "acquit_dtte_x", "note_by", "descript", "note_dtte", "note_by2", 
                     "descript2", "note_dtte2", "note_by3", "descript3", "note_dtte3",
                     "note_by3", "descript3", "note_dtte3")

# 4. functions -----------------------------------------------------------------

last_day_of_mth <-
  function(year, month){
  ymd(paste0(year, "-", month, "-1")) %>% 
      ceiling_date(., "month") %>% 
      {.-days(1)}
  }


add_total <- function(dta, ntotalcols = 1) {
  dta2 <- bind_rows(dta, apply(dta[,-c(1:as.numeric(ntotalcols)), drop = F], 2, sum, na.rm = TRUE))
  dta2[, 1:ntotalcols] <- lapply(dta2[, 1:ntotalcols], as.character)
  dta2[nrow(dta2), 1:ntotalcols] <- ""
  dta2[nrow(dta2), 1] <- "Total"
  dta2
}
# end function ------------------------------------------------------------------

maxdate <- last_day_of_mth(year, month)


Sys.setlocale("LC_TIME", "French")

# 5. week_end programing -------------------------------------------------------
weekend_beginnings <- ymd_hms("2024-01-02 00:00:00") + weeks(0:51)
weekend_endings <- ymd_hms("2024-01-08 00:00:00") + weeks(0:51)
weekends <- weekend_beginnings %--% weekend_endings

# 6. Easter days ---------------------------------------------------------------
paque <- ymd(EasterMonday(as.numeric(year)))
ascension <- paque + ddays(39)
pentecote <- paque + ddays(49)

# 7. table of public days ------------------------------------------------------
public <- tribble(
  ~start_time,             ~end_time,
  paste0(year,"-01-01 00:00:00"), paste0(year,"-01-02 00:00:00"), # Jour de l'an
  paste0(year,"-04-17 00:00:00"), paste0(year,"-04-18 00:00:00"), # Journée de la femme
  paste0(year,"-05-01 00:00:00"), paste0(year,"-05-02 00:00:00"), # Fête du Travail
  paste0(year,"-08-15 00:00:00"), paste0(year,"-08-18 00:00:00"), # Jour de l'indépendance
  paste0(year,"-11-01 00:00:00"), paste0(year,"-11-02 00:00:00"), # Toussaint
  paste0(year,"-12-25 00:00:00"), paste0(year,"-12-26 00:00:00"), # Noël
  paste0(paque, " 00:00:00"), paste0(paque + ddays(1), " 00:00:00"), # Lundi de Pâques
  paste0(ascension, " 00:00:00"), paste0(ascension + ddays(1), " 00:00:00"), # Ascension
  paste0(pentecote, " 00:00:00"), paste0(pentecote + ddays(1), " 00:00:00"), # Pentecote
  paste0(ymd(Ramadan), " 00:00:00"), paste0(ymd(Ramadan) + ddays(1), " 00:00:00"), # Eid al-Fitr
  paste0(ymd(Tabasky), " 00:00:00"), paste0(ymd(Tabasky) + ddays(1), " 00:00:00") # Eid al-Adha
  )
pub_days <- public$start_time %--% public$end_time

# 8. collected every nonworking day --------------------------------------------
days_off <- c(weekends, pub_days)

# 9. Data  manupulation --------------------------------------------------------

data <- 
  database %>%
  select(alarm_dtte_x : note_dtte2) %>%
  filter(zone != "") %>%
  mutate(alarm_dtte = parse_date_time(alarm_dtte_x, "%d-%b-%Y %H:%M:%S"),
         acquit_dtte = parse_date_time(note_dtte, "%d-%b-%Y %H:%M:%S"),
         date = as.Date(alarm_dtte)) %>%
  filter(date <= maxdate) %>% 
  mutate(id = row_number(),
         timespan = alarm_dtte %--% acquit_dtte)

alarm_log <-
  crossing(data, days_off) %>%
  mutate(daysoff_intersection = intersect(timespan, days_off),
         days_off = round(int_length(daysoff_intersection) / 86400, 1)) %>%
  group_by(id, date, alarm_dtte, acquit_dtte, timespan, alarm_type, sonde, zone,
           value, alarm_limit, acquit_by) %>%
  summarise(days_off = sum(days_off, na.rm = TRUE), .groups = "drop") %>%
  ungroup() %>%
  mutate(total_days = round(int_length(timespan) / 86400, 1),
         nbr_days = total_days - days_off,
         diff_enday = as.numeric(round(difftime(max(alarm_dtte), alarm_dtte, units = "days"), 1)),
         ndays = if_else(is.na(nbr_days), diff_enday, nbr_days),
         lab = case_when(str_detect(zone, "_Clinical Lab") ~ "Clinical Lab",
                         str_detect(zone, "Research Lab") ~ "IBM lab",
                         str_detect(zone, "_Pharmacy") ~ "Pharmacy",
                         str_detect(zone, "_Parasitology") ~ "Parasitology Lab",
                         str_detect(zone, "TBL") ~ "TB lab",
                         str_detect(zone, "_MicroBio") ~ "MicroBiology Lab",
                         str_detect(zone, "_TB Lab") ~ "TB Lab",
                         str_detect(zone, "Archive") ~ "Salle Archive",
                         TRUE ~ "other"),
         acquitted = if_else(acquit_by == "N/D", "No", "Yes"),
         acquit_after24 = case_when(acquitted == "Yes" & ndays <= 1 ~ "No",
                              acquitted == "Yes" & ndays > 1 ~ "Yes",
                              acquitted == "No" & ndays > 1 ~ "Yes"),
         temp = paste0(value, " / ", alarm_limit)) %>%
  select(date, alarm_dtte, alarm_type, sonde, lab, temp, acquitted, ndays, acquit_after24)

# 10. summary ------------------------------------------------------------------
log_2 <- alarm_log %>%
  mutate(type = if_else(alarm_type == "Écoulé", "Ecoulé Alarms", "Temperature Alarms")) %>%
  group_by(lab, type) %>%
  summarise(n = n(),
            n24 = sum(acquit_after24 == "Yes", na.rm = T),
            not = sum(acquitted == "No", na.rm = T)) %>%
  ungroup()

# 11. Render -------------------------------------------------------------------

## Starting
repstartd <- min(alarm_log$date)

rependd <- max(alarm_log$date)

repdays <- as.numeric(difftime(rependd, repstartd, units = "days"))
## -----------------------------------------------------------------------------

year <- as.numeric(str_extract(path_csv, "20[0-9]{2}"))
yearmonth <- str_extract(path_csv, "20[0-9]{2}-[0-9]{2}")

dir_report <- fs::path("reports", year, yearmonth)
fs::dir_create(dir_report)

fileAlertRmd <- "alert_log.Rmd"

lab_index <- unique(alarm_log$lab)

for (i in lab_index){
  output_DIR <- paste0(dir_report, paste0("/IceSpy Alert Report_", i, "_", format(as.Date(rependd), "%Y-%m")))
  rmarkdown::render(fileAlertRmd, output_file = output_DIR, quiet = TRUE)
}

# End --------------------------------------------------------------------------
